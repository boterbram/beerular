import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Beers, Beer} from '../models/beer';
import {MatTableDataSource} from '@angular/material';
import 'rxjs/add/operator/map';

@Injectable()
export class BeersService {
    dataObserver = this.http.get<Beers>('/assets/db.json');
    // FIXME: Tabledata should move back into beers-navigator again, pagination can only be set once.
    tableData = new MatTableDataSource();
    filterPredicate;
    filters = {};
    filterValues = {};

    constructor(private http: HttpClient) {}

    getData() {
        return this.dataObserver.map(data => data.bieren);
    }

    getTableData() {
        return this.getData().map((beers) => {
            this.tableData.data = beers;
            return this.tableData;
        });
    }

    registerFilter(property, filter: object) {
        this.filters[property] = filter;
        this.updateFilters();
    }

    updateFilters() {
        const filters = this.filters;
        const filterValues = this.filterValues;

        function combinedFilters(beer) {

            for (const property of Object.keys(filters)) {
                if (filterValues[property]
                    && filterValues[property].length > 0
                    && !filters[property](beer, filterValues[property])) {
                    return false;
                }
            }
            return true;
        }

        this.filterPredicate = combinedFilters;
    }

    // removeFilter(type: string) {
    //     delete this.filters[type];
    // }

    filter(property, value) {
        this.filterValues[property] = value;
        this.tableData.filterPredicate = this.filterPredicate;
        // FIXME: hack to trigger the filtering, I don't use the filter string in my filter.
        this.tableData.filter = 'a';
    }

    addBeer(beer: Beer) {
        this.tableData.data.push(beer);
        // Refresh the table since we are not actually saving to the database
        this.filter('', '');
    }
}
