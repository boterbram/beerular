export interface Beers {
    'bieren': Beer[];
}

export interface Beer {
    'id': number;
    'Bier': string;
    'Stijl': string;
    'Stamwortgehalte': number;
    'Alcoholpercentage': string;
    'Gisting': string;
    'Sinds': number;
    'Brouwerij': string;
}
