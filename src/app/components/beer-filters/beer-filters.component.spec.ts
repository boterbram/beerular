import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerFiltersComponent } from './beer-filters.component';

describe('BeerFiltersComponent', () => {
  let component: BeerFiltersComponent;
  let fixture: ComponentFixture<BeerFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeerFiltersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
