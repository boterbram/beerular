import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeersNavigatorComponent } from './beers-navigator.component';

describe('BeersNavigatorComponent', () => {
  let component: BeersNavigatorComponent;
  let fixture: ComponentFixture<BeersNavigatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeersNavigatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeersNavigatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
