import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource, MatPaginator, MatSort, MatDialog} from '@angular/material';
import {BeersService} from '../../services/beers.service';
import {BeerAdderComponent} from '../beer-adder/beer-adder.component';

@Component({
    selector: 'app-beers-navigator',
    templateUrl: './beers-navigator.component.html',
    styleUrls: ['./beers-navigator.component.scss']
})
export class BeersNavigatorComponent implements OnInit {
    tableData = new MatTableDataSource();
    tableColumns = ['Bier', 'Stijl'];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private beersService: BeersService, public dialog: MatDialog) {
    }

    ngOnInit() {

        this.beersService.getTableData().subscribe(data => {
            this.tableData = data;
            this.tableData.paginator = this.paginator;
            this.tableData.sort = this.sort;
        });
    }

    openAdder() {
        this.dialog.open(BeerAdderComponent, {
            width: '250px',
        });
    }
}
