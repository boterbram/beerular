import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerFilterTypeComponent } from './beer-filter-type.component';

describe('BeerFilterTypeComponent', () => {
  let component: BeerFilterTypeComponent;
  let fixture: ComponentFixture<BeerFilterTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeerFilterTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerFilterTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
