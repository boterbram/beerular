import {Component, OnInit} from '@angular/core';
import {BeersService} from '../../../services/beers.service';
import {FormControl} from '@angular/forms';
import {Beer} from '../../../models/beer';

@Component({
    selector: 'app-beer-filter-type',
    templateUrl: './beer-filter-type.component.html',
    styleUrls: ['./beer-filter-type.component.scss']
})
export class BeerFilterTypeComponent implements OnInit {
    types: string[];
    typesControl = new FormControl();

    constructor(private beersService: BeersService) {
    }

    ngOnInit() {
        this.types = [];
        this.getTypes();

        function typeFilter(beer: Beer, filterTypes: string[]) {
            return filterTypes.indexOf(beer.Stijl) > -1;
        }

        this.beersService.registerFilter('type', typeFilter);
    }

    getTypes() {
        this.beersService.getData().subscribe((beers) => {
            beers.map((beer) => {
                if (this.types.indexOf(beer.Stijl) < 0
                    && beer.Stijl.trim() !== '') {
                    this.types.push(beer.Stijl);
                }
            });
        });
    }

    filterBeers(types: string[]) {
        this.beersService.filter('type', types);
    }

}
