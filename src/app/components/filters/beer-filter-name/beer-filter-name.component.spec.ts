import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerFilterNameComponent } from './beer-filter-name.component';

describe('BeerFilterNameComponent', () => {
  let component: BeerFilterNameComponent;
  let fixture: ComponentFixture<BeerFilterNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeerFilterNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerFilterNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
