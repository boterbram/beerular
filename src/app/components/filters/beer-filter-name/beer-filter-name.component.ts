import {Component, OnInit} from '@angular/core';
import {BeersService} from '../../../services/beers.service';
import {Beer} from '../../../models/beer';

@Component({
    selector: 'app-beer-filter-name',
    templateUrl: './beer-filter-name.component.html',
    styleUrls: ['./beer-filter-name.component.scss']
})
export class BeerFilterNameComponent implements OnInit {

    constructor(private beersService: BeersService) {
    }

    ngOnInit() {
        function nameFilter(beer: Beer, filterString: string) {
            let name = beer.Bier;
            name = name.trim();
            name = name.toLowerCase();
            filterString = filterString.trim();
            filterString = filterString.toLowerCase();
            return name.indexOf(filterString) > -1;
        }

        this.beersService.registerFilter('name', nameFilter);
    }

    filterBeers(value: string) {
        this.beersService.filter('name', value);
    }

}
