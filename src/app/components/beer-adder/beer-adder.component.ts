import {Component, OnInit, Inject} from '@angular/core';
import {Beer} from '../../models/beer';
import {BeersService} from '../../services/beers.service';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'app-beer-adder',
    templateUrl: './beer-adder.component.html',
    styleUrls: ['./beer-adder.component.scss']
})
export class BeerAdderComponent implements OnInit {
    form: FormGroup;
    beer: Beer;

    constructor(private beersService: BeersService,
                public formBuilder: FormBuilder,
                public dialogRef: MatDialogRef<BeerAdderComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() {
        this.beer = {
            'id': 2,
            'Bier': 'Alfa Edel Pils',
            'Stijl': 'pilsener',
            'Stamwortgehalte': 12.0,
            'Alcoholpercentage': '5,0%',
            'Gisting': 'laag',
            'Sinds': null,
            'Brouwerij': 'Alfa'
        };

        this.form = this.formBuilder.group({
            Bier: ['', Validators.required],
            Stijl: ['', Validators.required]
        });
    }

    addBeer() {
        this.beersService.getData().subscribe((data) => {
            this.beer.id = data.length + 1;

            for (const value of Object.keys(this.form.value)) {
                this.beer[value] = this.form.value[value];
            }
            this.beersService.addBeer(this.beer);
            this.dialogRef.close();
        });
    }
}
