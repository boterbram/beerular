import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerAdderComponent } from './beer-adder.component';

describe('BeerAdderComponent', () => {
  let component: BeerAdderComponent;
  let fixture: ComponentFixture<BeerAdderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeerAdderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerAdderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
