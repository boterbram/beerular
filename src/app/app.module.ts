import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

// Material
import {
    MatButtonModule,
    MatTableModule,
    MatInputModule,
    MatSelectModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatDialogModule
} from '@angular/material';

// Custom
import {BeersService} from './services/beers.service';
import {AppComponent} from './app.component';
import {BeersNavigatorComponent} from './components/beers-navigator/beers-navigator.component';
import { BeerFiltersComponent } from './components/beer-filters/beer-filters.component';
import { BeerFilterNameComponent } from './components/filters/beer-filter-name/beer-filter-name.component';
import { BeerFilterTypeComponent } from './components/filters/beer-filter-type/beer-filter-type.component';
import { BeerAdderComponent } from './components/beer-adder/beer-adder.component';


@NgModule({
    declarations: [
        AppComponent,
        BeersNavigatorComponent,
        BeerFiltersComponent,
        BeerFilterNameComponent,
        BeerFilterTypeComponent,
        BeerAdderComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        MatButtonModule,
        MatTableModule,
        MatInputModule,
        MatSelectModule,
        MatSortModule,
        MatPaginatorModule,
        MatFormFieldModule,
        MatDialogModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [BeersService],
    entryComponents: [BeerAdderComponent],
    bootstrap: [AppComponent]
})
export class AppModule {
}
